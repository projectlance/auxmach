package com.android.auxmach.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.android.auxmach.R;
import com.android.auxmach.fragments.objects.BilgePumpObjectFragment;
import com.android.auxmach.fragments.objects.HeatEngineFragment;
import com.android.auxmach.fragments.objects.HeatExchangeFragment;
import com.android.auxmach.fragments.objects.OilubricatorFragment;
import com.android.auxmach.fragments.objects.PistonPumpFragment;

import com.android.auxmach.fragments.objects.ValveObjectFragment;

import com.android.auxmach.utilities.tools.CustomUtils;
import com.wang.avi.AVLoadingIndicatorView;

import org.rajawali3d.IRajawaliDisplay;
import org.rajawali3d.renderer.RajawaliRenderer;
import org.rajawali3d.surface.IRajawaliSurface;
import org.rajawali3d.surface.IRajawaliSurfaceRenderer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by vidalbenjoe on 25/02/2016.
 */
public abstract class ObjectsHolderFragments extends Fragment implements IRajawaliDisplay, View.OnClickListener {


    final int numParticles = 5000;

    float[] vertices = new float[numParticles * 3];
    float[] velocity = new float[numParticles * 3];
    float[] textureCoords = new float[numParticles * 2];
    float[] normals = new float[numParticles * 3];
    float[] colors = new float[numParticles * 4];
    short[] indices = new short[numParticles];

    int index = 0;


    public static final String BUNDLE_EXAMPLE_URL = ObjectsHolderFragments.class.getSimpleName();
    public static final String BUNDLE_EXAMPLE_TITLE = "BUNDLE_EXAMPLE_TITLE";

    protected ProgressBar mProgressBarLoader;
    protected AVLoadingIndicatorView avloadingIndicatorView;
    protected String mExampleUrl;
    protected String mExampleTitle;
    protected RelativeLayout mLayout;
    protected IRajawaliSurface mRajawaliSurface;
    protected IRajawaliSurfaceRenderer mRenderer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        final Bundle bundle = getArguments();
//        if (bundle == null || !bundle.containsKey(BUNDLE_EXAMPLE_URL)) {
//            throw new IllegalArgumentException(getClass().getSimpleName()
//                    + " requires " + BUNDLE_EXAMPLE_URL
//                    + " argument at runtime!");
//        }
//
//        mExampleUrl = bundle.getString(BUNDLE_EXAMPLE_URL);
//        mExampleTitle = bundle.getString(BUNDLE_EXAMPLE_TITLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // Inflate the view
        mLayout = (RelativeLayout) inflater.inflate(getLayoutID(), container, false);

        mLayout.findViewById(R.id.relative_layout_loader_container).bringToFront();
        ImageView bigelpump = (ImageView) mLayout.findViewById(R.id.bigelpumpImg);
        ImageView valbeObj = (ImageView) mLayout.findViewById(R.id.valbeObjImg);
        ImageView heatengineImg = (ImageView) mLayout.findViewById(R.id.heatengineImg);
        ImageView heatexchangeImg = (ImageView) mLayout.findViewById(R.id.heatexchangeImg);
        ImageView oillubricatorImg = (ImageView) mLayout.findViewById(R.id.oillubricatorImg);
        ImageView pistonpumpImg = (ImageView) mLayout.findViewById(R.id.pistonpumpImg);


        bigelpump.setOnClickListener(this);
        valbeObj.setOnClickListener(this);
        heatengineImg.setOnClickListener(this);
        heatexchangeImg.setOnClickListener(this);
        oillubricatorImg.setOnClickListener(this);
        pistonpumpImg.setOnClickListener(this);

        // Find the TextureView
        mRajawaliSurface = (IRajawaliSurface) mLayout.findViewById(R.id.rajwali_surface);

        // Create the loader
        mProgressBarLoader = (ProgressBar) mLayout.findViewById(R.id.progress_bar_loader);
        mProgressBarLoader.setVisibility(View.GONE);

        avloadingIndicatorView = (AVLoadingIndicatorView) mLayout.findViewById(R.id.avloadingIndicatorView);


        // Create the renderer
        mRenderer = createRenderer();
        onBeforeApplyRenderer();
        applyRenderer();
        return mLayout;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bigelpumpImg:
                CustomUtils.loadFragment(new BilgePumpObjectFragment(), true, getActivity());
                break;
            case R.id.valbeObjImg:
                CustomUtils.loadFragment(new ValveObjectFragment(), true, getActivity());
                break;
            case R.id.heatengineImg:
                CustomUtils.loadFragment(new HeatEngineFragment(), true, getActivity());
                break;
            case R.id.heatexchangeImg:
                CustomUtils.loadFragment(new HeatExchangeFragment(), true, getActivity());
                break;
            case R.id.oillubricatorImg:
                CustomUtils.loadFragment(new OilubricatorFragment(), true, getActivity());
                break;

            case R.id.pistonpumpImg:
                CustomUtils.loadFragment(new PistonPumpFragment(), true, getActivity());
                break;

        }
    }

    protected void onBeforeApplyRenderer() {

    }

    protected void applyRenderer() {
        mRajawaliSurface.setSurfaceRenderer(mRenderer);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mLayout != null)
            mLayout.removeView((View) mRajawaliSurface);
    }

    @Override
    public int getLayoutID() {
        return R.layout.rajawali_textureview_layout;
    }

    protected void hideLoader() {
        mProgressBarLoader.post(new Runnable() {
            @Override
            public void run() {
                mProgressBarLoader.setVisibility(View.GONE);
                avloadingIndicatorView.setVisibility(View.GONE);
            }
        });
    }

    protected void showLoader() {
        mProgressBarLoader.post(new Runnable() {
            @Override
            public void run() {
                mProgressBarLoader.setVisibility(View.VISIBLE);
                avloadingIndicatorView.setVisibility(View.VISIBLE);
            }
        });
    }

    protected abstract class AMObjectsRendered extends RajawaliRenderer {

        public AMObjectsRendered(Context context) {
            super(context);
        }

        @Override
        public void onOffsetsChanged(float v, float v2, float v3, float v4, int i, int i2) {

        }

        @Override
        public void onTouchEvent(MotionEvent event) {

        }

        @Override
        public void onRenderSurfaceCreated(EGLConfig config, GL10 gl, int width, int height) {
            showLoader();
            super.onRenderSurfaceCreated(config, gl, width, height);
            hideLoader();
        }

        @Override
        protected void onRender(long ellapsedRealtime, double deltaTime) {
            super.onRender(ellapsedRealtime, deltaTime);
        }
    }
}
