package com.android.auxmach.fragments.topics;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.auxmach.R;
import com.android.auxmach.database.AssessmentDB.QUIZDBAdapter;
import com.android.auxmach.database.AssessmentDB.SessionCache;
import com.android.auxmach.fragments.assessments.AuxMachAssesmentActivity;
import com.android.auxmach.fragments.objects.ValveObjectFragment;
import com.android.auxmach.utilities.tools.CustomUtils;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;


public class Fragment_Topic_Contents extends Fragment {

    QUIZDBAdapter myDb;
    SessionCache QuizSession;
    int retake;
    int prevTotal;
    int curTotal;

    Intent intent;
    String initVal = "1";
    String finalDate;

    String strCategory, strContent, strContentTitle, strPosition;
    LinearLayout linearExitButtons;
    View lineSeparator;
    Button takeAssessmentBt, view3dObjBt;
    Dialog dialog;
    private YoYo.YoYoString rope;
    SharedPreferences sp;
    public static String file = "Files";
    Bundle bundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = getActivity().getSharedPreferences(file, 0);
        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();
        /** Getting integer data of the key current_page from the bundle */
        strCategory = data.getString("category");
        strContentTitle = data.getString("title");
        strContent = data.getString("content");
        strPosition = data.getString("position");

        intent = new Intent();
        QuizSession = new SessionCache(getActivity());
        openDB();

        Date date = new Date();
        SimpleDateFormat timeFormat = new SimpleDateFormat("MMM dd, yyyy");
        finalDate = timeFormat.format(date);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_topic_contents, container, false);
        if (sp.getString("isfirstlunch", "").contentEquals("")) {
            displayTutorial();
        }
        takeAssessmentBt = (Button) v.findViewById(R.id.takeAssessmentBt);
        view3dObjBt = (Button) v.findViewById(R.id.view3dObjBt);

        Log.i("Category", strCategory);

        TextView txtContentTitle = (TextView) v.findViewById(R.id.txtContentTitle);
        txtContentTitle.setText(strContentTitle);
        lineSeparator = (View) v.findViewById(R.id.lineSeparator);
        TextView txtContent = (TextView) v.findViewById(R.id.tvContents);
        txtContent.setText(strContent);
        linearExitButtons = (LinearLayout) v.findViewById(R.id.linearExitButtons);
        if (strContentTitle.equals("") && strContent.equals("")) {
            linearExitButtons.setVisibility(View.VISIBLE);
            lineSeparator.setVisibility(View.GONE);
            takeAssessmentBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("tapped: ", "take assess");
                    Log.i("hasFlQuiz2: ", "" + QuizSession.hasFlQuiz1());

                    if (QuizSession.hasFlQuiz1()) {
                        final Dialog dialog = new Dialog(getActivity(), R.style.DialogAnim);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.validate_message);
                        Button bYes = (Button) dialog.findViewById(R.id.buttonOk);
                        Button bNo = (Button) dialog.findViewById(R.id.buttonCancel);
                        TextView tvalertmessage = (TextView) dialog.findViewById(R.id.tvalertmessage);
                        HashMap<String, String> quizRecord = QuizSession.getTotalSum();
                        retake = Integer.parseInt(quizRecord.get(SessionCache.REPEATING1));
                        prevTotal = Integer.parseInt(quizRecord.get(SessionCache.AUX_MAX_ITEM1));

//                        CustomUtils.setQuestionCategory(CustomUtils.getQuestionCategory());
                        Log.i("popasodpad: ", CustomUtils.getQuestionCategory());
                        Log.i("retaikes: ", "" + retake);
                        Log.i("prevToto: ", "" + prevTotal);
                        Log.i("quizRecord: ", "" + quizRecord);

                        if (retake >= 3) {
                            tvalertmessage.setText("You have taken this " + retake + " times, Do you want to take this quiz again?");
                            bYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // delete the record
                                    myDb.deleteQuiz(CustomUtils.getQuestionCategory());
                                    // store last quiz session for JS and for all the records
                                    QuizSession.StoreFlLastQuizTaken(finalDate);
                                    QuizSession.StoreAllLastQuizTaken(finalDate);
                                    // delete the scorerow if the
                                    // user wants to
                                    // overwrite the first take of
                                    // quiz
//                                    myDb.deletescorerowSet(1, CustomUtils.getQuestionCategory() + " 1");
                                    // get the retake value + 1
                                    // sum is 4 so when the user try
                                    // to take the quiz
                                    // again, he will not able to
                                    // take it any more, he
                                    // will the next condition which
                                    // will appear
                                    // "You have taken this 4 times"
                                    int sum = retake + 1;
                                    Log.i("getQuestionCategory: ", "" + CustomUtils.getQuestionCategory());
                                    Log.i("sumOClick: ", "" + sum);
                                    myDb.addAUXquiz(1, CustomUtils.getQuestionCategory(), "", "0 %");
                                    QuizSession.FinishSessionNum1(Integer.toString(sum));
                                    intent = new Intent(getActivity(), AuxMachAssesmentActivity.class);
//                                    bundle.putString("questcat", CustomUtils.getQuestionCategory());
//                                    intent.putExtras(bundle);
                                    intent.putExtra("retakeNum", sum);
                                    startActivity(intent);
                                    dialog.dismiss();
                                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                }
                            });
                            bNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            // this condition will use if retake is
                            // value 1 to 2
                            myDb.deleteQuiz(CustomUtils.getQuestionCategory());
                            QuizSession.StoreFlLastQuizTaken(finalDate);
                            QuizSession.StoreAllLastQuizTaken(finalDate);
                            int sum = retake + 1;
                            myDb.addAUXquiz(1, CustomUtils.getQuestionCategory(), "", "0 %");
                            curTotal = prevTotal + 5;
                            QuizSession.StoreTotal1(Integer.toString(curTotal));
                            QuizSession.FinishSessionNum1(Integer.toString(sum));
                            intent = new Intent(getActivity(), AuxMachAssesmentActivity.class);

                            Log.i("sstrCategory: ", "" + strCategory);
//                            bundle.putString("questcat", CustomUtils.getQuestionCategory());
//                            intent.putExtras(bundle);
                            Log.i("sumElse: ", "" + sum);
                            intent.putExtra("retakeNum", sum);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

                        }
                    } else {
                        QuizSession.StoreFlLastQuizTaken(finalDate);
                        QuizSession.StoreAllLastQuizTaken(finalDate);
                        int passVal = Integer.parseInt(initVal);
                        myDb.addAUXquiz(1, CustomUtils.getQuestionCategory(), initVal, "0 %");
                        curTotal = prevTotal + 5;
                        QuizSession.StoreTotal1(Integer.toString(curTotal));
                        QuizSession.FinishSessionNum1(initVal);
                        bundle = new Bundle();
                        intent = new Intent(getActivity(), AuxMachAssesmentActivity.class);
                        bundle.putString("questcat", strCategory);
                        intent.putExtras(bundle);
                        Log.i("passValSum: ", "" + passVal);
                        intent.putExtra("retakeNum", passVal);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);

                    }
                }
            });

            view3dObjBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("tapped: ", "3dview");
                    CustomUtils.loadFragment(new ValveObjectFragment(), true, getActivity());
                }
            });
        }

        return v;
    }

    void displayTutorial() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_tutorial);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }

        ImageView imgTut = (ImageView) dialog.findViewById(R.id.tutorialImg);
        final ImageView handImg = (ImageView) dialog.findViewById(R.id.handImg);
        handImg.setBackgroundResource(R.drawable.swipeimagehand);
        imgTut.setBackgroundResource(R.drawable.swiperightleft);

        rope = YoYo.with(Techniques.FadeInLeft)
                .duration(1200)
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                        animation.setDuration(1200);
                        animation.setTarget(handImg);
                    }
                })
                .playOn(handImg);

        imgTut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                try {
                    SharedPreferences.Editor ed = sp.edit();
                    ed.putString("isfirstlunch", "1");
                    ed.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

    private void openDB() {
        myDb = new QUIZDBAdapter(getActivity());
        myDb.open();
    }


}
