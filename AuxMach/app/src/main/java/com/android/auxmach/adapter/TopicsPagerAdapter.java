package com.android.auxmach.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.android.auxmach.fragments.topics.Fragment_Topic_Contents;
import com.android.auxmach.utilities.tools.CustomUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class TopicsPagerAdapter extends FragmentStatePagerAdapter {

    public static int PAGE_COUNT;
    ArrayList<HashMap<String, String>> arrayList;

    /**
     * Constructor of the class
     */
    public TopicsPagerAdapter(FragmentManager fm, ArrayList<HashMap<String, String>> arrayList) {
        super(fm);
        this.arrayList = arrayList;
        PAGE_COUNT = this.arrayList.size();
    }

    /**
     * 0
     * This method will be invoked when a page is requested to create
     */
    @Override
    public Fragment getItem(int arg0) {
        Fragment_Topic_Contents myFragment = new Fragment_Topic_Contents();
        Bundle data = new Bundle();
        try {
            data.putString("category", arrayList.get(arg0).get("category"));
            data.putString("content", arrayList.get(arg0).get("content"));
            data.putString("title", arrayList.get(arg0).get("title"));
            data.putString("position", String.valueOf(arg0));
            CustomUtils.setQuestionCategory(arrayList.get(arg0).get("category"));
        } catch (Exception e) {
            data.putString("category", "");
            data.putString("content", "");
            data.putString("title", "");
            data.putString("position", String.valueOf(arg0));
        }

        myFragment.setArguments(data);
        return myFragment;
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount() {
        return arrayList.size() + 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page #" + (position + 1);
    }


}
