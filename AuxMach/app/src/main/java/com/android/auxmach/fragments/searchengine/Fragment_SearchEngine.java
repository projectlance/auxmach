package com.android.auxmach.fragments.searchengine;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.android.auxmach.R;
import com.android.auxmach.adapter.ListViewAdapter;
import com.android.auxmach.database.DatabaseAccess;
import com.android.auxmach.model.SearchEngineModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by luigigo on 3/1/16.
 */
public class Fragment_SearchEngine extends Fragment {
    View mainView;
    ArrayList<SearchEngineModel> array;
    ArrayList<HashMap<String, String>> originalArray;
    DatabaseAccess databaseAccess;
    ProgressDialog mProgressDialog;
    ListViewAdapter adapter;
    ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseAccess = DatabaseAccess.getInstance(getActivity());

        originalArray = databaseAccess.searchContents();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_search_engine, container, false);

        final EditText edtSearch = (EditText) mainView.findViewById(R.id.edtSearch);
        listView = (ListView) mainView.findViewById(R.id.lvResults);
      //  new AsyncTaskDB().execute();

        array = new ArrayList<SearchEngineModel>();
        for (int i = 0; i < originalArray.size(); i++) {
            SearchEngineModel searchEngineModel = new SearchEngineModel();
            searchEngineModel.setCategory(originalArray.get(i).get("category"));
            searchEngineModel.setTitle(originalArray.get(i).get("title"));
            searchEngineModel.setContent(originalArray.get(i).get("content"));
            searchEngineModel.setImageBanner(originalArray.get(i).get("image"));
            array.add(searchEngineModel);
        }

        adapter = new ListViewAdapter(getActivity(), array);
        listView.setAdapter(adapter);

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }


            @Override
            public void afterTextChanged(Editable editable) {
                Log.i("ArrayList: ", array.size() + "");
                Log.i("ArrayList- ", array.size() + "");
                String text = edtSearch.getText().toString().toLowerCase(Locale.getDefault());
                adapter.filter(text);
            }
        });
        return mainView;
    }


    // RemoteDataTask AsyncTask
    private class AsyncTaskDB extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(getActivity());
            // Set progressdialog title
            mProgressDialog.setTitle("Opening Database");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create the array

            Log.i("ARRAYSIZE", array.size() + "");
            try {


            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            mProgressDialog.dismiss();
        }
    }
}


