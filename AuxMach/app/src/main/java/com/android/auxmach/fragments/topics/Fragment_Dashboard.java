package com.android.auxmach.fragments.topics;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.auxmach.R;
import com.android.auxmach.database.AssessmentDB.QUIZDBAdapter;
import com.android.auxmach.database.DatabaseAccess;
import com.android.auxmach.fragments.searchengine.Fragment_SearchEngine;
import com.android.auxmach.model.Faculty;
import com.android.auxmach.utilities.tools.BackEndlessSettings;
import com.android.auxmach.utilities.tools.CustomUtils;
import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by luigigo on 2/23/16.
 */
public class Fragment_Dashboard extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    View mainView;
    Button btnTopics, btn3DObjects;
    BarChart bcQuizResultPreview;
    QUIZDBAdapter database;
    ArrayList<HashMap<String, String>> originalData;
    Spinner spnCategory;
    String spinnerStr;
    public static boolean isRestricted = false;


    DatabaseAccess databaseAccess;
    ArrayList<HashMap<String, String>> arrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new QUIZDBAdapter(getActivity());
        Backendless.initApp(getActivity(), BackEndlessSettings.APPLICATION_ID, BackEndlessSettings.SECRET_KEY, BackEndlessSettings.VERSION);
        showRestrictionDialog();
//        database.open();
//        originalData = database.getScores("Air Compressor ");
//        database.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        Backendless.initApp(getActivity(), BackEndlessSettings.APPLICATION_ID, BackEndlessSettings.SECRET_KEY, BackEndlessSettings.VERSION);
        databaseAccess = DatabaseAccess.getInstance(getActivity());


        btnTopics = (Button) mainView.findViewById(R.id.btnTopics);
        btnTopics.setOnClickListener(this);
        btn3DObjects = (Button) mainView.findViewById(R.id.btn3dSimulator);
        btn3DObjects.setOnClickListener(this);
        spnCategory = (Spinner) mainView.findViewById(R.id.spnCategory);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_spinner, getResources().getStringArray(R.array.titles1));
        spnCategory.setAdapter(spinnerAdapter);
        spnCategory.setOnItemSelectedListener(this);
        spnCategory.setPrompt("Select item");

        // showRestrictionDialog();

//        setupChart();

        return mainView;
    }

    private void showRestrictionDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_restriction);
        dialog.setCancelable(false);
        dialog.show();
        final EditText edtAccessCode = (EditText) dialog.findViewById(R.id.edtAccessCode);
        final Spinner spnRestrictionType = (Spinner) dialog.findViewById(R.id.spnRestrictionType);
        spnRestrictionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spnRestrictionType.getSelectedItemPosition() == 0) {
                    edtAccessCode.setVisibility(View.GONE);
                } else {
                    edtAccessCode.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        Button btnRestriction = (Button) dialog.findViewById(R.id.btnRestriction);
        btnRestriction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spnRestrictionType.getSelectedItem().equals("Student")) {
                    isRestricted = false;
                    dialog.dismiss();

                } else {
                    CustomUtils.showDialog(getActivity());
                    QueryOptions queryOptions = new QueryOptions();
                    queryOptions.setRelated(Arrays.asList("accesscode"));
                    BackendlessDataQuery query = new BackendlessDataQuery(queryOptions);
                    Backendless.Data.of(Faculty.class).find(query, new AsyncCallback<BackendlessCollection<Faculty>>() {

                        @Override
                        public void handleResponse(BackendlessCollection<Faculty> listOfRecords) {
                            Faculty faculty = new Faculty();
                            faculty.setAccesscode(listOfRecords.getCurrentPage().get(0).getAccesscode());
                            Log.i("AccessCode", listOfRecords.getCurrentPage().get(0).getAccesscode());

                            if (edtAccessCode.getText().toString().equals(faculty.getAccesscode())) {
                                isRestricted = true;
                                dialog.dismiss();
                            } else {
                                Toast.makeText(getActivity(), "Invalid Access Code", Toast.LENGTH_SHORT).show();
                            }
                            CustomUtils.hideDialog();

                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            Log.i("AccessCode (Fault)", fault.getMessage());
                            Toast.makeText(getActivity(), "Can't retrieve data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                            CustomUtils.hideDialog();
                        }
                    });

                }
            }
        });
    }

    private void setupChart() {
        bcQuizResultPreview = (BarChart) mainView.findViewById(R.id.bcQuizResultPreview);
        bcQuizResultPreview.setDescriptionColor(Color.WHITE);
        bcQuizResultPreview.getAxisRight().setValueFormatter(new MyValueFormatter());

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        bcQuizResultPreview.setMaxVisibleValueCount(60);
        bcQuizResultPreview.setBackgroundColor(getResources().getColor(R.color.theme_color_background));
        bcQuizResultPreview.setNoDataTextDescription("You need to provide data for the chart.");
        // scaling can now only be done on x- and y-axis separately
        bcQuizResultPreview.setDrawBarShadow(false);
        bcQuizResultPreview.setDrawGridBackground(false);
        bcQuizResultPreview.getAxisLeft().setDrawLabels(false);
        bcQuizResultPreview.getAxisRight().setTextColor(Color.WHITE); // left y-axis
        bcQuizResultPreview.getXAxis().setTextColor(Color.WHITE);
        bcQuizResultPreview.getLegend().setTextColor(Color.WHITE);


        XAxis xAxis = bcQuizResultPreview.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.WHITE);
        xAxis.setSpaceBetweenLabels(0);
        xAxis.setDrawGridLines(false);

        bcQuizResultPreview.getAxisLeft().setDrawGridLines(false);

        // add a nice and smooth animation
        bcQuizResultPreview.setDescription(spnCategory.getSelectedItem().toString());
        bcQuizResultPreview.animateY(2500);
        bcQuizResultPreview.getLegend().setEnabled(false);


        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        for (int i = 0; i < originalData.size(); i++) {
            yVals1.add(new BarEntry(Integer.parseInt(originalData.get(i).get("spoint")), i));
        }

        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < originalData.size(); i++) {
            xVals.add(originalData.get(i).get("usernamescore"));
        }

        BarDataSet set1 = new BarDataSet(yVals1, "Data Set");
        set1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        set1.setDrawValues(false);

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);
        data.setValueFormatter(new MyValueFormatter());
        data.setValueTextColor(Color.WHITE);

        bcQuizResultPreview.setData(data);
        bcQuizResultPreview.invalidate();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnTopics:
                CustomUtils.loadFragment(new Fragment_Topics(), true, getActivity());
                break;
            case R.id.btn3dSimulator:
                CustomUtils.loadFragment(new Fragment_SearchEngine(), true, getActivity());
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        spinnerStr = spnCategory.getSelectedItem().toString();

        if (spinnerStr.toString().isEmpty() || spinnerStr == null || spinnerStr.toString() == "" || spinnerStr.contentEquals("")) {
            Toast.makeText(getActivity(), "Please select an item", Toast.LENGTH_SHORT).show();
        } else {
            database.open();
            originalData = database.getScores(spnCategory.getSelectedItem().toString());
            database.close();
            setupChart();

            for (IDataSet set : bcQuizResultPreview.getData().getDataSets())
                set.setDrawValues(!set.isDrawValuesEnabled());

            bcQuizResultPreview.invalidate();
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class MyValueFormatter implements ValueFormatter, YAxisValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return Math.round(value) + "/5";
        }

        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return Math.round(value) + "";
        }
    }
}
