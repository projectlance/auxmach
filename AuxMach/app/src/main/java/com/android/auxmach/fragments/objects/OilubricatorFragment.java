package com.android.auxmach.fragments.objects;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.auxmach.R;
import com.android.auxmach.fragments.ObjectsHolderFragments;

import com.android.auxmach.utilities.tools.CustomUtils;

import org.rajawali3d.Object3D;
import org.rajawali3d.animation.Animation;
import org.rajawali3d.animation.EllipticalOrbitAnimation3D;
import org.rajawali3d.cameras.ArcballCamera;
import org.rajawali3d.cameras.Camera;
import org.rajawali3d.lights.DirectionalLight;
import org.rajawali3d.loader.LoaderOBJ;
import org.rajawali3d.loader.ParsingException;
import org.rajawali3d.materials.Material;
import org.rajawali3d.materials.methods.DiffuseMethod;
import org.rajawali3d.math.vector.Vector3;
import org.rajawali3d.util.debugvisualizer.DebugCamera;
import org.rajawali3d.util.debugvisualizer.DebugLight;
import org.rajawali3d.util.debugvisualizer.DebugVisualizer;
import org.rajawali3d.util.debugvisualizer.GridFloor;

/**
 * Created by vidalbenjoe on 26/02/2016.
 */
public class OilubricatorFragment extends ObjectsHolderFragments {

    @Override
    public AMObjectsRendered createRenderer() {
        return new ArcballCameraRenderer(getActivity());
    }

    public final class ArcballCameraRenderer extends AMObjectsRendered {

        public ArcballCameraRenderer(Context context) {
            super(context);
        }

        LoaderOBJ MachineObject;
        Object3D objMachine;
        private Camera mOtherCamera;

        @Override
        protected void initScene() {
            try {
                loadModel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void loadModel() throws ParsingException {
            DirectionalLight light = new DirectionalLight();
            light.setLookAt(1, -1, 1);
            light.enableLookAt();
            light.setPower(1.5f);
            getCurrentScene().addLight(light);

            light = new DirectionalLight();
            light.setLookAt(-1, 1, -1);
            light.enableLookAt();
            light.setPower(1.5f);
            getCurrentScene().addLight(light);

            getCurrentScene().setBackgroundColor(0x393939);

            mOtherCamera = new Camera();
            mOtherCamera.setPosition(4, 2, -10);
            mOtherCamera.setFarPlane(10);
            mOtherCamera.enableLookAt();

            DebugVisualizer debugViz = new DebugVisualizer(this);
            debugViz.addChild(new GridFloor(20, 0x555555, 1, 20));
            debugViz.addChild(new DebugLight(light, 0x999900, 1));
            debugViz.addChild(new DebugCamera(mOtherCamera, 0x000000, 1));
            getCurrentScene().addChild(debugViz);


            MachineObject = new LoaderOBJ(this, R.raw.oillubricator);
            MachineObject.parse();
            objMachine = MachineObject.getParsedObject();


            objMachine.getScale();

            objMachine.setScaleX(0.0100);
            objMachine.setScaleY(0.0100);
            objMachine.setScaleZ(0.0100);

            Log.i("3dscale:", "" + objMachine.getScale());

            Material material = new Material();
            material.enableLighting(true);
            material.setDiffuseMethod(new DiffuseMethod.Lambert());
//            material.setColor(0x1f1f1e);

//            try {
//                material.addTexture(new Texture("earthColors", R.drawable.starry_night));
//            } catch (ATexture.TextureException e) {
//                e.printStackTrace();
//            }
//            material.setColorInfluence(0);

            objMachine.setMaterial(material);
            getCurrentScene().addChild(objMachine);

            ArcballCamera arcball = new ArcballCamera(mContext, ((Activity) mContext).findViewById(R.id.drawer_layout));
            arcball.setPosition(4, 4, 4);
            getCurrentScene().replaceAndSwitchCamera(getCurrentCamera(), arcball);

        }
        private void animateCamera() {
            getCurrentCamera().enableLookAt();
            getCurrentCamera().setLookAt(0, 0, 0);

            EllipticalOrbitAnimation3D a = new EllipticalOrbitAnimation3D(new Vector3(), new Vector3(20,
                    10, 20), Vector3.getAxisVector(Vector3.Axis.Y), 0, 360,
                    EllipticalOrbitAnimation3D.OrbitDirection.CLOCKWISE);
            a.setDurationMilliseconds(20000);
            a.setRepeatMode(Animation.RepeatMode.INFINITE);
            a.setTransformable3D(getCurrentCamera());
            getCurrentScene().registerAnimation(a);
            a.play();
        }

    }


}