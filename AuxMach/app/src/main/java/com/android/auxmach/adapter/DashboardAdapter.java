package com.android.auxmach.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.auxmach.R;
import com.android.auxmach.fragments.topics.Fragment_Topics;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;

public class DashboardAdapter extends BaseAdapter {

    ArrayList<HashMap<String, String>> _list;
    Activity _activity;
    LayoutInflater inflater;
    ViewHolder viewHolder;

    public DashboardAdapter(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
        _activity = activity;
        _list = arrayList;
    }

    @Override
    public int getCount() {
        return _list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            viewHolder = new ViewHolder();
            inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_item, viewGroup, false);

            viewHolder.title = (TextView) view.findViewById(R.id.list_item_title);
            viewHolder.image = (ImageView) view.findViewById(R.id.list_item_image);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.title.setText(_list.get(i).get("title"));
        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strTitle = String.valueOf(_list.get(i).get("title"));
                String strImageId = String.valueOf(_list.get(i).get("image"));
                Fragment_Topics fragment_topics = new Fragment_Topics();
                fragment_topics.openDetails(view, strImageId, strTitle);
            }
        });

        Glide.with(view.getContext())
                .load(Integer.parseInt(_list.get(i).get("image")))
                .dontTransform()
                .dontAnimate()
                .into(viewHolder.image);
        return view;
    }

    private class ViewHolder {
        ImageView image;
        TextView title;
    }

}
