package com.android.auxmach.fragments.topics;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.auxmach.R;
import com.android.auxmach.utilities.tools.GUI.TouchImageView;

import java.util.Locale;

/**
 * Created by luigigo on 3/3/16.
 */
public class Fragment_Topic_Preview extends Fragment implements View.OnClickListener, TextToSpeech.OnInitListener {

    View mainView;
    Button btnTTS;
    TouchImageView imgImageBanner;
    TextView txtCategory, txtTitle, txtContent, txtImageIndicator;
    String strCategory, strTitle, strContent, strImageBanner;
    TextToSpeech myTTS;
    boolean isSpeaking = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_topic_preview, container, false);

        Bundle bundleFromListviewAdapter = this.getArguments();

        strCategory = bundleFromListviewAdapter.getString("category");
        strTitle = bundleFromListviewAdapter.getString("title");
        strContent = bundleFromListviewAdapter.getString("content");
        strImageBanner = bundleFromListviewAdapter.getString("image");
        initializeVars();

        return mainView;
    }

    private void initializeVars() {
        txtCategory = (TextView) mainView.findViewById(R.id.txtCategory);
        txtTitle = (TextView) mainView.findViewById(R.id.txtTitle);
        txtContent = (TextView) mainView.findViewById(R.id.txtContent);
        txtImageIndicator = (TextView) mainView.findViewById(R.id.txtImageIndicator);
        imgImageBanner = (TouchImageView) mainView.findViewById(R.id.imgImageBanner);


        myTTS = new TextToSpeech(getActivity(), this);
        btnTTS = (Button) mainView.findViewById(R.id.btnTTS);
        btnTTS.setOnClickListener(this);

        txtCategory.setText(strCategory);
        txtTitle.setText(strTitle);
        txtContent.setText(strContent);

        if (strImageBanner != null) {
            int resId = getResources().getIdentifier(strImageBanner, "drawable", getActivity().getPackageName());
            imgImageBanner.setImageResource(resId);
        } else {
            txtImageIndicator.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnTTS:
                String strCategory, strTitle, strContent;
                strCategory = txtCategory.getText().toString();
                strTitle = txtTitle.getText().toString();
                strContent = txtContent.getText().toString();


                if (isSpeaking) {
                    isSpeaking = false;
                    myTTS.stop();
                } else {
                    isSpeaking = true;
                    myTTS.speak(strCategory + "." + strTitle + "." + strContent, TextToSpeech.QUEUE_FLUSH, null);
                }

                break;
        }
    }

    @Override
    public void onInit(int i) {

        //check for successful instantiation
        if (i == TextToSpeech.SUCCESS) {
            if (myTTS.isLanguageAvailable(Locale.UK) == TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.UK);
        } else if (i == TextToSpeech.ERROR) {
            Toast.makeText(getActivity(), "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //the user has the necessary data - create the TTS

            } else {
                //no data - install it now
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }

    @Override
    public void onDestroyView() {
        myTTS.stop();
        super.onDestroyView();
    }
}
