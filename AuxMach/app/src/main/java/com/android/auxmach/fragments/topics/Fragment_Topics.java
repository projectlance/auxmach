package com.android.auxmach.fragments.topics;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.alexvasilkov.foldablelayout.UnfoldableView;
import com.alexvasilkov.foldablelayout.shading.GlanceFoldShading;
import com.android.auxmach.R;
import com.android.auxmach.adapter.DashboardAdapter;
import com.android.auxmach.adapter.TopicsPagerAdapter;
import com.android.auxmach.database.DatabaseAccess;
import com.android.auxmach.model.Topic;
import com.android.auxmach.utilities.tools.BackEndlessSettings;
import com.android.auxmach.utilities.tools.CustomUtils;
import com.android.auxmach.utilities.tools.GUI.TouchImageView;
import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;
import com.backendless.persistence.QueryOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * ip
 * Created by luigigo on 2/25/16.
 */
public class Fragment_Topics extends Fragment implements View.OnClickListener, TextToSpeech.OnInitListener {

    private View mListTouchInterceptor;
    private static View mDetailsLayout;
    public static UnfoldableView mUnfoldableView;
    ArrayList<HashMap<String, String>> arrayList;
    static String[] databaseTitles;
    int[] databaseImageBanners;
    View mainView;
    static TopicsPagerAdapter topicsPagerAdapter;
    static FragmentManager fm;

    static DatabaseAccess databaseAccess;
    static ArrayList<HashMap<String, String>> arrayListTopic, arrayCatcher;
    Button btnVideo, btnTextToSpeech, btnAddLesson, btnUpdateLesson;

    private int position = 0;
    VideoView resultsVideo;
    private MediaController mediaControls;
    private static ProgressDialog progressDialog;
    Dialog videoDialog;
    static String strTitleIndicator, strBackendIndicator;
    static ViewPager pager;
    static TextView txtTitle, linearTxtContent, linearTxtContentTitle;
    static LinearLayout linearFrontView, linearOptionsButton, linearOptionsButtonAddUpdate;
    public static TextToSpeech myTTS;
    int page = 0;
    public static boolean isSpeaking = false;
    static TouchImageView image;
    static Button btnViewMore;
    static RelativeLayout relativeBanner;
    int lastId = 0;
    Dialog dialogBackend;

    static ArrayList<HashMap<String, String>> arraylist;
    static Toast toast;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Backendless.initApp(context, BackEndlessSettings.APPLICATION_ID, BackEndlessSettings.SECRET_KEY, BackEndlessSettings.VERSION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.activity_topics, container, false);

        if (mediaControls == null) {

            mediaControls = new MediaController(getActivity());
        }

        CustomUtils.checkTTSdata(getActivity());

        // Initializing Database
        databaseAccess = DatabaseAccess.getInstance(getActivity());

        // Referencing Fragment Manager
        fm = getFragmentManager();

        toast = Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 130);


        progressDialog = new ProgressDialog(getActivity());
        btnVideo = (Button) mainView.findViewById(R.id.btnVideo);
        btnVideo.setOnClickListener(this);
        myTTS = new TextToSpeech(getActivity(), this);
        btnTextToSpeech = (Button) mainView.findViewById(R.id.btnTTS);
        btnTextToSpeech.setOnClickListener(this);
        btnAddLesson = (Button) mainView.findViewById(R.id.btnAddLesson);
        btnAddLesson.setOnClickListener(this);
        btnUpdateLesson = (Button) mainView.findViewById(R.id.btnUpdateLesson);
        btnUpdateLesson.setOnClickListener(this);

        arrayList = new ArrayList<HashMap<String, String>>();
        databaseTitles = getResources().getStringArray(R.array.titles);
        databaseImageBanners = new int[]{
                R.drawable.banner_aircompressor,
                R.drawable.banner_airdistribution,
                R.drawable.banner_reciprocating_compressor,
                R.drawable.banner_heat_engine,
                R.drawable.banner_heat_exchanger,
                R.drawable.banner_operate_pumping_system,
                R.drawable.banner_storage_compressed_air
        };

        for (int i = 0; i < databaseTitles.length; i++) {
            HashMap<String, String> hm = new HashMap<String, String>();
            hm.put("title", databaseTitles[i]);
            hm.put("image", String.valueOf(databaseImageBanners[i]));
            arrayList.add(hm);
        }


        DashboardAdapter dashboardAdapter = new DashboardAdapter(getActivity(), arrayList);
        ListView listView = (ListView) mainView.findViewById(R.id.list_view);
        listView.setAdapter(dashboardAdapter);

        mListTouchInterceptor = mainView.findViewById(R.id.touch_interceptor_view);
        mListTouchInterceptor.setClickable(false);

        mDetailsLayout = mainView.findViewById(R.id.details_layout);
        mDetailsLayout.setVisibility(View.INVISIBLE);

        relativeBanner = (RelativeLayout) mDetailsLayout.findViewById(R.id.relativeBanner);
        linearOptionsButton = (LinearLayout) mDetailsLayout.findViewById(R.id.linearOptionsButton);
        linearOptionsButtonAddUpdate = (LinearLayout) mDetailsLayout.findViewById(R.id.linearOptionsButtonAddUpdate);
        image = (TouchImageView) mDetailsLayout.findViewById(R.id.details_image);
        linearTxtContent = (TextView) mDetailsLayout.findViewById(R.id.tvContents);
        linearTxtContentTitle = (TextView) mDetailsLayout.findViewById(R.id.txtContentTitle);
        btnViewMore = (Button) mDetailsLayout.findViewById(R.id.btnViewMore);
        btnViewMore.setOnClickListener(this);
        linearFrontView = (LinearLayout) mDetailsLayout.findViewById(R.id.linearFrontView);

        if (!Fragment_Dashboard.isRestricted) {
            linearOptionsButtonAddUpdate.setVisibility(View.GONE);
        }

        pager = (ViewPager) mDetailsLayout.findViewById(R.id.pager);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                //setupImageBanner(position);

                int count = TopicsPagerAdapter.PAGE_COUNT;
                Log.i("POSITION: " + pager.getCurrentItem(), " Count: " + count);
                if (pager.getCurrentItem() != count) {
                    page = position;
                    try {
                        String strImageBanner = arrayCatcher.get(page + 1).get("image");

                        if (strImageBanner != null) {
                            int resId = getResources().getIdentifier(strImageBanner, "drawable", getActivity().getPackageName());
                            relativeBanner.setVisibility(View.VISIBLE);
                            image.setImageResource(resId);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (isSpeaking) {
                        isSpeaking = false;
                        myTTS.stop();
                    }
                    relativeBanner.setVisibility(View.VISIBLE);
                    linearOptionsButton.setVisibility(View.VISIBLE);
                    if (!Fragment_Dashboard.isRestricted) {
                        linearOptionsButtonAddUpdate.setVisibility(View.GONE);
                    } else {
                        linearOptionsButtonAddUpdate.setVisibility(View.VISIBLE);
                    }
                } else {
                    relativeBanner.setVisibility(View.GONE);
                    linearOptionsButton.setVisibility(View.GONE);
                    linearOptionsButtonAddUpdate.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mUnfoldableView = (UnfoldableView) mainView.findViewById(R.id.unfoldable_view);
        mUnfoldableView.setGesturesEnabled(false);

        Bitmap glance = BitmapFactory.decodeResource(getResources(), R.drawable.unfold_glance);
        mUnfoldableView.setFoldShading(new GlanceFoldShading(glance));

        mUnfoldableView.setOnFoldingListener(new UnfoldableView.SimpleFoldingListener() {
            @Override
            public void onUnfolding(UnfoldableView unfoldableView) {
                mListTouchInterceptor.setClickable(true);
                mDetailsLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onUnfolded(UnfoldableView unfoldableView) {
                mListTouchInterceptor.setClickable(false);
            }

            @Override
            public void onFoldingBack(UnfoldableView unfoldableView) {
                mListTouchInterceptor.setClickable(true);
            }

            @Override
            public void onFoldedBack(UnfoldableView unfoldableView) {
                mListTouchInterceptor.setClickable(false);
                mDetailsLayout.setVisibility(View.INVISIBLE);
                page = 0;
                linearFrontView.setVisibility(View.VISIBLE);
                relativeBanner.setVisibility(View.VISIBLE);
                myTTS.stop();
            }
        });
        return mainView;
    }

    public void showVideo() {

        videoDialog = new Dialog(getActivity());
        videoDialog.setTitle("Title");
        videoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        videoDialog.setContentView(R.layout.dialog_video);
        videoDialog.setCancelable(true);
        videoDialog.show();
        resultsVideo = (VideoView) videoDialog.findViewById(R.id.resultsVideo);
        txtTitle = (TextView) videoDialog.findViewById(R.id.txtTitle);
        txtTitle.setText(strTitleIndicator);
        Button okVid = (Button) videoDialog.findViewById(R.id.videoOKBt);
        resultsVideo.setZOrderOnTop(true);
//         create a progress bar while the video file is loading
        // set a title for the progress bar
        progressDialog.setTitle("Video");
        // set a message for the progress bar
        progressDialog.setMessage("Loading...");
        //set the progress bar not cancelable on users' touch
        progressDialog.setCancelable(true);
        // show the progress bar
        progressDialog.show();

        try {

            //set the media controller in the VideoView
            resultsVideo.setMediaController(mediaControls);
            switch (strTitleIndicator) {
                case "Air Compressor":
                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_air_compressor));
                    break;

                case "Air Distribution":
                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_air_distribution));
                    break;

                case "Construction Details of Reciprocating Compressors":
                    final ListView listView = new ListView(getActivity());
                    listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, new String[]{"Fuel and Oil System", "Hyrdraulic Pump", "Oil Water Separator"}));
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.setContentView(listView);
                    dialog.setTitle("Select video below:");
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            switch (i) {


                                case 0:
                                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_fuel_and_oil_system));
                                    dialog.dismiss();
                                    break;

                                case 1:
                                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_hydraulic_pump));
                                    dialog.dismiss();
                                    break;

                                case 2:
                                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_oil_water_separator));
                                    dialog.dismiss();
                                    break;
                            }

                        }
                    });
                    dialog.show();
                    break;

                case "Heat Engine Cycle":
                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_heat_engine_cycle));
                    break;

                case "Heat Exchanger":
                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_heat_exchanger));
                    break;
                case "Operate Pumping Systems and Associated Control Systems":
                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_pumping_system));
                    break;
                case "Storage of Compressed Air":
                    resultsVideo.setVideoURI(Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.vid_storage_compressed_air));
                    break;

                default:
                    Toast.makeText(getActivity(), "No video available", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                    videoDialog.dismiss();
                    break;
            }

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();

        }

//        we also set an setOnPreparedListener in order to know when the video file is ready for playback
        resultsVideo.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {
                // close the progress bar and play the video
                progressDialog.dismiss();
                //if we have a position on savedInstanceState, the video playback should start from here
                resultsVideo.seekTo(position);
                if (position == 0) {
                    resultsVideo.start();
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    resultsVideo.pause();
                }
            }
        });

        okVid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoDialog.dismiss();
            }
        });

    }

    public void openDetails(View coverView, String imageId, String title) {
        image.setImageResource(Integer.parseInt(imageId));

        databaseAccess.open();
        strTitleIndicator = title;

        progressDialog.setTitle("Retrieving data from server");
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setRelated(Arrays.asList("category", "title", "content", "image"));
        queryOptions.addSortByOption("id ASC");
        queryOptions.setPageSize(100);
        BackendlessDataQuery query = new BackendlessDataQuery(queryOptions);
        query.setWhereClause("category = " + "'" + strTitleIndicator + "'");
        Backendless.Data.of(Topic.class).find(query, new AsyncCallback<BackendlessCollection<Topic>>() {

            @Override
            public void handleResponse(BackendlessCollection<Topic> listOfRecords) {


                Log.i("FAULTS", listOfRecords.getCurrentPage().size() + " ");
                BackendlessCollection<Topic> restaurants = listOfRecords;
                List<Topic> totalList = new ArrayList<Topic>();
                arraylist = new ArrayList<HashMap<String, String>>();
                totalList.addAll(restaurants.getCurrentPage());

                for (Topic topic : totalList) {
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm.put("id", topic.getId());
                    hm.put("category", topic.getCategory());
                    hm.put("title", topic.getTitle());
                    hm.put("content", topic.getContent());
                    hm.put("image", topic.getImage());
                    arraylist.add(hm);
                }

                Log.i("FAULTS", listOfRecords.getCurrentPage().size() + " arraylist");
                arrayListTopic = new ArrayList<HashMap<String, String>>();
                arrayCatcher = arraylist;

                for (int i = 1; i < arrayCatcher.size(); i++) {
                    HashMap<String, String> hm = new HashMap<String, String>();
                    hm.put("category", arrayCatcher.get(i).get("category"));
                    hm.put("title", arrayCatcher.get(i).get("title"));
                    hm.put("content", arrayCatcher.get(i).get("content"));
                    arrayListTopic.add(hm);
                }

                linearTxtContentTitle.setText(arrayCatcher.get(0).get("title"));
                linearTxtContent.setText(arrayCatcher.get(0).get("content"));
                topicsPagerAdapter = new TopicsPagerAdapter(fm, arrayListTopic);
                pager.setAdapter(topicsPagerAdapter);
                progressDialog.dismiss();

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.i("FAULTS", fault.getMessage());
                progressDialog.dismiss();
                toast.show();

            }
        });

        databaseAccess.close();

        Log.i("TOPIC TITLE", title);
        mUnfoldableView.unfold(coverView, mDetailsLayout);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnVideo:
                if (isSpeaking) {
                    isSpeaking = false;
                    myTTS.stop();
                }
                showVideo();
                break;

            case R.id.btnTTS:
                try {
                    if (isSpeaking) {
                        isSpeaking = false;
                        myTTS.stop();
                    } else {
                        isSpeaking = true;
                        if (linearFrontView.getVisibility() == View.GONE)
                            myTTS.speak(String.valueOf(arrayListTopic.get(page).get("title") + "." + arrayListTopic.get(page).get("content")), TextToSpeech.QUEUE_FLUSH, null);
                        else
                            myTTS.speak(String.valueOf(arrayCatcher.get(0).get("title") + "." + arrayCatcher.get(0).get("content")), TextToSpeech.QUEUE_FLUSH, null);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Device can't read the content", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnViewMore:
                linearFrontView.setVisibility(View.GONE);
                if (isSpeaking) {
                    isSpeaking = false;
                    myTTS.stop();
                }
                break;

            case R.id.btnAddLesson:
                try {
                    strBackendIndicator = "save";
                    CustomUtils.showDialog(getActivity());
                    getBackendLastId();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.btnUpdateLesson:
                try {
                    strBackendIndicator = "update";
                    if (linearFrontView.getVisibility() == View.VISIBLE) {
                        showAddUpdateLessonDialog(page, "Updating Lesson");
                    } else {
                        showAddUpdateLessonDialog(page + 1, "Updating Lesson");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    private void getBackendLastId() {

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.setRelated(Arrays.asList("category", "title", "content", "image"));
        queryOptions.addSortByOption("id DESC");
        queryOptions.setPageSize(100);
        BackendlessDataQuery query = new BackendlessDataQuery(queryOptions);
        Backendless.Data.of(Topic.class).find(query, new AsyncCallback<BackendlessCollection<Topic>>() {

            @Override
            public void handleResponse(BackendlessCollection<Topic> listOfRecords) {
                Log.i("ADDLESSON: (SUCCESS)", listOfRecords.getCurrentPage().size() + " " + listOfRecords.getCurrentPage().get(0).getId());
                lastId = Integer.parseInt(listOfRecords.getCurrentPage().get(0).getId());
                showAddUpdateLessonDialog(page, "Creating New Lesson");
                CustomUtils.hideDialog();

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.i("ADDLESSON: (FAULT)", fault.getMessage());
                Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                CustomUtils.hideDialog();

            }
        });
    }

    private void showAddUpdateLessonDialog(final int page, String strHeader) {
        Log.i("BackendCurrentId", arrayCatcher.get(page).get("id"));


        dialogBackend = new Dialog(getActivity());
        dialogBackend.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBackend.setContentView(R.layout.dialog_add_lesson);
        dialogBackend.show();

        TextView txtHeader = (TextView) dialogBackend.findViewById(R.id.txtHeader);
        txtHeader.setText(strHeader);
        final Spinner edtCategory = (Spinner) dialogBackend.findViewById(R.id.edtCategory);
        final EditText edtTitle = (EditText) dialogBackend.findViewById(R.id.edtTitle);
        final EditText edtContent = (EditText) dialogBackend.findViewById(R.id.edtContent);
        Button btnSaveLesson = (Button) dialogBackend.findViewById(R.id.btnSaveLesson);
        btnSaveLesson.setText(strBackendIndicator);
        btnSaveLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strCategory = edtCategory.getSelectedItem().toString();
                String strTitle = edtTitle.getText().toString();
                String strContent = edtContent.getText().toString();

                CustomUtils.showDialog(getActivity());
                if (strBackendIndicator.equals("save")) {
                    addLesson(strCategory, strTitle, strContent);
                } else {
                    updateLesson(page, strCategory, strTitle, strContent);
                }
            }
        });

    }

    private void updateLesson(int page, final String category, final String title, final String content) {
        // This block of code is for updating the data within the backend


        String whereClause = "id = " + arrayCatcher.get(page).get("id");
        BackendlessDataQuery dataQuery = new BackendlessDataQuery();
        dataQuery.setWhereClause(whereClause);
        Backendless.Data.of(Topic.class).find(dataQuery, new AsyncCallback<BackendlessCollection<Topic>>() {
            @Override
            public void handleResponse(BackendlessCollection<Topic> response) {

                Log.i("ResSize: ", "" + response.getCurrentPage().size());
                Topic topic = response.getCurrentPage().get(0);
                topic.setCategory(category);
                topic.setTitle(title);
                topic.setContent(content);

                Backendless.Data.of(Topic.class).save(topic, new AsyncCallback<Topic>() {
                    @Override
                    public void handleResponse(Topic response) {
                        Toast.makeText(getActivity(), "Record successfully updated", Toast.LENGTH_SHORT).show();
                        dialogBackend.dismiss();
                        mUnfoldableView.foldBack();
                        CustomUtils.hideDialog();
                        Toast.makeText(getActivity(), "Topic needs to refresh its content", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void handleFault(BackendlessFault fault) {
                        Log.i("ErrMsg: ", fault.getMessage());
                        Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                        CustomUtils.hideDialog();
                    }
                });
            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Log.i("ErrMsg: ", fault.getMessage());
                Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                CustomUtils.hideDialog();
            }
        });
    }

    private void addLesson(String category, String title, String content) {

        // This line of code is for saving data into backendless
        Topic topic = new Topic();
        topic.setId(String.valueOf(lastId + 1));
        topic.setCategory(category);
        topic.setTitle(title);
        topic.setContent(content);

        // save object asynchronously
        Backendless.Persistence.of(Topic.class).save(topic, new AsyncCallback<Topic>() {

            @Override
            public void handleResponse(Topic response) {
                Toast.makeText(getActivity(), "Record successfully saved", Toast.LENGTH_SHORT).show();
                dialogBackend.dismiss();
                mUnfoldableView.foldBack();
                CustomUtils.hideDialog();
                Toast.makeText(getActivity(), "Topic needs to refresh its content", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void handleFault(BackendlessFault fault) {
                Toast.makeText(getActivity(), "Can't retreive data. Please check internet connection.", Toast.LENGTH_SHORT).show();
                CustomUtils.hideDialog();
            }
        });
    }


    @Override
    public void onInit(int i) {

        //check for successful instantiation
        if (i == TextToSpeech.SUCCESS) {
            if (myTTS.isLanguageAvailable(Locale.UK) == TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.UK);
        } else if (i == TextToSpeech.ERROR) {
            Toast.makeText(getActivity(), "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //the user has the necessary data - create the TTS

            } else {
                //no data - install it now
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }
}
